var gulp = require('gulp');
var fs = require('fs');
var path = require('path');
var replace = require('gulp-replace');
var rimraf = require('rimraf');
var request = require('request');
var critical = require('critical');
var config = require('../config.js');
var configLocal = require('../gulpconfig.local.js');

// Allow requests to work with non-valid SSL certificates.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Generate & Inline Critical-path CSS
gulp.task('critical', function (cb) {
    critical.generate({
        base: config.criticalcss.base,
        inline: config.criticalcss.inline,
        css: config.criticalcss.css,
        width: config.criticalcss.width,
        height: config.criticalcss.height,
        minify: config.criticalcss.minify,
        src: 'http://testsite.dev.digity.be/',
        dest: 'themes/custom/basetheme/css/critical/page.css'
    }),
    critical.generate({
        base: config.criticalcss.base,
        inline: config.criticalcss.inline,
        css: config.criticalcss.css,
        width: config.criticalcss.width,
        height: config.criticalcss.height,
        minify: config.criticalcss.minify,
        src: 'http://testsite.dev.digity.be/contacteer-ons',
        dest: 'themes/custom/basetheme/css/critical/contactpagina.css'
    });
});
