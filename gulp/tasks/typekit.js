/* Create a local copy of the typekit JS file */

/**
 * plugins
 */
var gulp = require('gulp'),
    download = require('gulp-download');

/**
 * configfile
 */
var config = require('../config').typekit;

/**
 * Tasks
 */
gulp.task('typekit', function() {
    return download(config.src)
        .pipe(gulp.dest(config.dest));
});
