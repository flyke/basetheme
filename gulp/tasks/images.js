/* SVG task */

/**
 * plugins
 */
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var imageminPngquant = require('gulp-pngquant');
var responsive = require('gulp-responsive');

/**
 * configfile
 */
var config = require('../config').images;

/**
 * Tasks
 */

// Compress all jpg, gif and png images from images/src into images/dest.
gulp.task('compress-images', function() {
    gulp.src(config.compress.src)

        .pipe(imagemin([
            imagemin.gifsicle({interlaced: config.compress.interlaced}),
            imageminJpegRecompress({
                progressive: config.compress.progressive,
                max: config.compress.maxquality,
                min: config.compress.minquality
            }),
            imageminPngquant({quality: config.compress.pngquality})
        ], {
            verbose: config.compress.verbose
        }))

        .pipe(gulp.dest(config.dest));
});

// Make derivatives for most common screen resolutions for all background images.
gulp.task('create-backgrounds', function() {

    gulp.src(config.backgrounds.src).pipe(responsive({
        'bg-*.jpg': [{
            width: 2500,
            rename: { suffix: '-2500px' },
        }, {
            width: 1920,
            rename: { suffix: '-1920px' },
        }, {
            width: 1440,
            rename: { suffix: '-1440px' },
        }, {
            width: 1366,
            rename: { suffix: '-1366px' },
        }, {
            width: 768,
            rename: { suffix: '-768px' },
        }, {
            width: 720,
            rename: { suffix: '-720px' },
        }, {
            width: 375,
            rename: { suffix: '-375px' },
        }, {
            width: 360,
            rename: { suffix: '-360px' },
        }]
    }, {
        // Global configuration for all images:
        // The output quality for JPEG, WebP and TIFF output formats.
        quality: config.backgrounds.quality,
        // Use progressive (interlace) scan for JPEG and PNG output.
        progressive: config.backgrounds.progressive,
        // Strip all metadata.
        withMetadata: config.backgrounds.keepmetadata
    }))

        .pipe(gulp.dest(config.backgrounds.dest));
});

gulp.task('images', function () {
    gulp.start('compress-images');
    gulp.start('create-backgrounds');
});
