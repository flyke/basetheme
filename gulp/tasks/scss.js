/* SCSS task */

/**
 * plugins
 */
var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    stylelint = require("stylelint"),
    reporter = require("postcss-reporter"),
    syntax = require("postcss-scss"),
    postcss = require('gulp-postcss'),
    flexbug = require('postcss-flexbugs-fixes'),
    triangle = require("postcss-triangle"),
    autoprefixer = require('autoprefixer'),
    browserSync = require('browser-sync'),
    cssnano = require('gulp-cssnano'),
    sourcemaps = require('gulp-sourcemaps');

/**
 * configfile
 */
var config = require('../config');

/**
 * Postcss processors
 */
var processors = [
    autoprefixer(config.scss.prefix),
    triangle,
    flexbug
];

/**
 * Tasks
 */
gulp.task("lint-styles", function () {
    gulp.src(config.scss.src)
        .pipe(plumber())
        .pipe(postcss([
            stylelint(config.scss.lint),
            reporter({clearMessages: true})
        ], {syntax: syntax}));
});

gulp.task('scss', ["lint-styles"], function () {
    gulp.src(config.scss.src)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass.sync(config.scss.settings)
            .pipe(sass())
            .on('error', sass.logError))
        .pipe(postcss(processors, {syntax: syntax}))
        .pipe(sourcemaps.write('.'))
        .pipe(browserSync.stream({match: '**/*.css'}))
        .pipe(gulp.dest(config.scss.dest))
});

gulp.task('scss-build', ["lint-styles"], function () {
    gulp.src(config.scss.src)
        .pipe(plumber())
        .pipe(sass.sync(config.scss.settings)
            .pipe(sass())
            .on('error', sass.logError))
        .pipe(postcss(processors, {syntax: syntax}))
        .pipe(cssnano({autoprefixer: false, zindex: false}))
        .pipe(browserSync.stream({match: '**/*.css'}))
        .pipe(gulp.dest(config.scss.dest))
});
