/* Watch task */

/**
 * plugins
 */
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    watch = require('gulp-watch');

/**
 * configs
 */
var config = require('../config');

/**
 * Tasks
 */
gulp.task('watch', ['build', 'browsersync'], function () {
    watch(config.scss.glob, function () {
        gulp.start('scss');
    });
    watch(config.svg.src, function () {
        gulp.start('svg');
        browserSync.reload();
    });
    watch(config.images.src, function () {
        gulp.start('images');
        browserSync.reload();
    });
    watch(config.files, function () {
        browserSync.reload();
    });
});
