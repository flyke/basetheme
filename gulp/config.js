/**
 * Config file
 */

var fs = require("fs");
var path = './';
var domain = 'http://testsite.dev.digity.be/';

module.exports = {

    /**
     * Scss settings
     */
    scss: {
        src: path + 'scss/*.scss',
        dest: path + 'dist/css/',
        glob: path + 'scss/**/*.scss',
        settings: {
            outputStyle: 'expanded'
        },
        lint: fs.readFileSync("scss/.stylelintrc", "utf8")
    },

    /**
     * Autoprefixer supported browsers
     */
    autoprefixer: {
        browsers: [
            'last 2 version',
            '> .5% in BE'
        ]
    },

    /**
     * Cssnano
     */
    cssnano: {
        autoprefixer: false
    },

    /**
     * SVG settings
     */
    svg: {
        svgsprite: {
            src: path + 'images/src/svg/sprites/**/*.svg',
            dest: path + 'dist/images/svg/',
            settings: {
                shape: {
                    spacing: {
                        padding: 0
                    },
                    dest: 'individual'
                },
                mode: {
                    view: {
                        bust: false,
                        render: {
                            scss: true
                        }
                    },
                    symbol: true
                }
            }
        },
        src: path + 'images/src/svg/**/*.svg',
        dest: path + 'dist/images/svg/',
        clean: path + 'dist/images/svg/**/*.svg'
    },

    /**
     * Other images settings (jpg, png,...)
     */
    images: {

        compress: {
            src: path + 'images/src/**/*.{jpg,png,gif}',
            interlaced: true,
            progressive: true,
            maxquality: 80,
            minquality: 70,
            pngquality: '75-85',
            verbose: true
        },


        backgrounds: {
            src: path + 'images/src/jpg/**/*.jpg',
            quality: 80,
            progressive: true,
            keepmetadata: false,
            dest: path + 'dist/images/jpg'
        },

        logos: {
            src: path + 'images/src/svg/sprites/**/logo-*.svg',
            dest: path + 'dist/images/png/',
            width: 500,
        },

        dest: path + 'dist/images/'
    },

    /**
     * BrowserSync settings
     */
    browsersync: {
        proxy: domain,
        notify: false
    },

    /**
     * Critical css
     */
    criticalcss: {
        base: '../../../',
        inline: false,
        css: ['dist/css/main.css'],
        width: 1300,
        height: 900,
        minify: true,
    },

    /**
     * Typekit
     */
    typekit: {
        src: 'https://use.typekit.net/' + 'vsw4epp' + '.js',
        dest: path + 'js/',
    },

    /**
     * Files
     */
    files: [path + 'templates/**/*.twig', path + '**/*.html', path + '**/*.js', path + '**/*.twig', '!' + path + '**/node_modules/**/*']
};
