/**
 * Initiate a swiper gallery on .js-gallery elements
 */

// Gallery settings.
var settings = {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    sliderSelector: '.js-slider',
};

/**
 * Initiate a slider on .js-slider elements
 */
if(document.getElementsByClassName(settings.sliderSelector.replace('.','')).length) {
    var slider = new Swiper(settings.sliderSelector, {
        watchOverflow: true,
        navigation: {
            nextEl: settings.nextButton,
            prevEl: settings.prevButton,
        },
        allowTouchMove: false,
    });
}