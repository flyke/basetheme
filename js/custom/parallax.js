/**
 * Initiate parallax effects. Normally this would happen automatically if elements have data-parallax and data-image-src
 * attributes. But for some reason that did not work, so we kickstart parallax here manually.
 * 
 */
(function ($) {
    $('.js-parallax').each( function(){
        $(this).parallax({imageSrc:  $(this).data('image-src')});
    });
})(jQuery);