/**
 * Make the header sticky while scrolling.
 * https://github.com/garand/sticky
 */
(function ($) {

    $(document).ready(function(){
        // Sticky header for anonymous users.
        if (!$('body').hasClass('toolbar-horizontal')) {
            $('.js-sticky').sticky({topSpacing:0});
        }
        // Sticky header for admin users.
        if ($('body').hasClass('toolbar-horizontal')) {
            $('.js-sticky').sticky({topSpacing:79});
        }

        $('.js-sticky').on('sticky-end', function() {
            $('#sticky-wrapper').css("height", "");
        });

    });

}(jQuery));
