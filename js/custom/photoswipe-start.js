/**
 * Init photoswipe
 */

(function ($, Drupal) {

    //initialize PhotoSwipe on page load and after ajax call (like selecting a product viarant) only once
    Drupal.behaviors.photoswipe = {

        settings: {
            gallerySelector: '.js-photoswipe',
            loadedClass: '.js-photoswipe-loaded',
            itemSelector: '.js-photoswipe-item',
            captionClass: '.caption',
        },

        init: function (context, settings) {
            // get dom element for photoswipe (this is boilerplate needed for owlcarousel)
            var pswpElement = document.querySelectorAll('.pswp')[0];

            //initiate each gallery separately so images of multiple galleries don't get merged together
            var gallery = [];
            $(Drupal.behaviors.photoswipe.settings.gallerySelector).each(function (i, e) {

                //set loaded class to reflect that photoswipe is active here and to prevent loading multiple times
                $(this).addClass(Drupal.behaviors.photoswipe.settings.loadedClass);

                // get slide data for each slide (url, title, size ...) and place it in array items
                var items = [];
                $(this).find(Drupal.behaviors.photoswipe.settings.itemSelector).each(function (index) {
                    var item = {};

                    if ($(this).is('img')) { //if we have an image
                        item.src = $(this).attr('data-target');
                        item.w = $(this).attr('data-width');
                        item.h = $(this).attr('data-height');
                        item.title = $(this).next(Drupal.behaviors.photoswipe.settings.captionClass).text();
                        item.msrc = $(this).attr('src');
                        items.push(item);
                    }
                });

                //when clicking on image, open gallery
                $(this).find(Drupal.behaviors.photoswipe.settings.itemSelector).click(function (event) {
                    // open image x in gallery. x is attribute index given by data-index attribute.
                    var options = {
                        index: parseInt($(this).attr('data-index')),
                        showAnimationDuration: 300,
                        showHideOpacity: true,
                        shareEl: false
                    };
                    gallery[i] = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                    gallery[i].init();
                });
            });

        },
        attach: function (context, settings) {
            Drupal.behaviors.photoswipe.init();
        }
    }

}(jQuery, Drupal));
