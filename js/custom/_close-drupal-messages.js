/**
 * Close drupal status message when clicking on the close button.
 */
(function ($) {

    var options = {
        selector: '.js-close-message',
        message: '.js-message',
        root: '.js-message-root'
    };

    var app = {
        init: function () {
            app.bindUIevents();
        },

        bindUIevents: function () {
            $(document).on('click', options.selector, function () {
                var scope = $(this).closest(options.root);
                var message = $(this).closest(options.message);
                app.closeMessage(message, scope);
            });
        },

        closeMessage: function (message, scope) {
            message.remove();
            app.checkVisibility(scope);
        },

        checkVisibility: function (scope) {
            var hasMessages = scope.find(options.message).length > 0;
            if (!hasMessages) {
                scope.hide();
            } else {
                scope.show();
            }
        }
    };

    app.init();

}(jQuery));
